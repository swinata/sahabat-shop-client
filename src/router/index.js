import Vue from 'vue';
import Router from 'vue-router';

const Home = () => import(/* webpackChunkName: "home" */ '@/components/Home');
const Login = () => import(/* webpackChunkName: "login" */ '@/components/Login');
const About = () => import(/* webpackChunkName: "about" */ '@/components/About');

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/about',
      name: 'About',
      component: About,
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
});
